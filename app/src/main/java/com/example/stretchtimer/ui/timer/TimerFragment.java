package com.example.stretchtimer.ui.timer;

import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.stretchtimer.databinding.FragmentTimerBinding;

public class TimerFragment extends Fragment {

    private FragmentTimerBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        TimerViewModel timerViewModel =
                new ViewModelProvider(this).get(TimerViewModel.class);

        binding = FragmentTimerBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textHome;
        final Chronometer chronometer = binding.chronometer;
        final Button startStopButton = binding.buttonStartStop;

        chronometer.setBase(SystemClock.elapsedRealtime());

        startStopButton.setOnClickListener(v -> {
            timerViewModel.startStopTimer();
            String buttonText = timerViewModel.isCountdown() ? "Stop" : "Start";
            startStopButton.setText(buttonText);
        });

        timerViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        timerViewModel.getText().observe(getViewLifecycleOwner(), chronometer::setText);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}