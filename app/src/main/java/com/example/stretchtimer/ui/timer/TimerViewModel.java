package com.example.stretchtimer.ui.timer;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Locale;

public class TimerViewModel extends ViewModel {

    private final MutableLiveData<String> mText;

    public boolean isCountdown() {
        return isCountdown;
    }

    private boolean isCountdown;
    private long startTime;

    public TimerViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("00:30");
        isCountdown = false;
    }

    public void startStopTimer() {
        if (isCountdown) {
            stopCountdown();
        } else {
            startCountdown();
        }
    }

    private void startCountdown() {
        isCountdown = true;
        long countdownTime = 30000; // 30 seconds
        startTime = SystemClock.elapsedRealtime() + countdownTime;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                long currentTime = SystemClock.elapsedRealtime();
                long elapsedTime = startTime - currentTime;
                int seconds = (int) (elapsedTime / 1000);
                if (seconds >= 0) {
                    String timeString = String.format(Locale.getDefault(), "00:%02d", seconds);
                    mText.setValue(timeString);
                    if (isCountdown) {
                        new Handler(Looper.getMainLooper()).postDelayed(this, 1000);
                    } else {
                        mText.setValue("00:30");
                    }
                } else {
                    stopCountdown();
                }
            }
        });
    }

    private void stopCountdown() {
        isCountdown = false;
    }

    public LiveData<String> getText() {
        return mText;
    }
}